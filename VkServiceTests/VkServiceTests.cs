﻿using System;
using NUnit.Framework;
using VkInfo;
using Moq;
using System.Collections.Generic;

namespace VkTests
{
    [TestFixture]
    public class VkServiceTests
    {
        #region Тесты состояния

        [Test]
        public void AuthorizationAvailable_ForUser()
        {
            var model = Mock.Of<IVkModel>(x => x.Login(null, null, 0) == true);
            var srv = new VkService(model);

            var result = srv.Model.Login(null, null, 0);

            Assert.IsTrue(result);
        }

        [Test]
        public void SendingMessageAvailable_ForUser()
        {
            var model = Mock.Of<IVkModel>(x => x.SendMessage(0, "Test Message") == true);
            var srv = new VkService(model);

            var result = srv.Model.SendMessage(0, "Test Message");

            Assert.IsTrue(result);
        }

        [Test]
        public void GettingFullNameById_ForUser_JhonSmith()
        {
            var model = Mock.Of<IVkModel>(x => x.GetFullName(12345678) == "John Smith");
            var srv = new VkService(model);

            var result = srv.Model.GetFullName(12345678);

            Assert.AreEqual("John Smith", result);
        }

        [Test]
        public void GettingUserPageUriById_ForUser_12345678()
        {
            var model = Mock.Of<IVkModel>(x => x.GetUserPageUri(12345678) == "vk.com/id12345678");
            var srv = new VkService(model);

            var result = srv.Model.GetUserPageUri(12345678);

            Assert.AreEqual("vk.com/id12345678", result);
        }

        [Test]
        public void GettingUserPageUriByScreenName_ForUser_jhon()
        {
            var model = Mock.Of<IVkModel>(x => x.GetUserPageUri("john") == "vk.com/john");
            var srv = new VkService(model);

            var result = srv.Model.GetUserPageUri("john");

            Assert.AreEqual("vk.com/john", result);
        }

        [Test]
        public void GettingActivityByGroupsById_ForUserId12345678_NoActivity()
        {
            var model = Mock.Of<VkModel>(x => x.GetGroupCount(12345678) == 0);

            var result = model.GetActivityByGroups(12345678);

            Assert.AreEqual("Нет активности", result);
        }

        [Test]
        public void GettingCurrentUserId_ForUser_12345678()
        {
            var model = Mock.Of<IVkModel>(x => x.GetCurrentUserId() == 12345678);
            var srv = new VkService(model);

            var result = srv.Model.GetCurrentUserId();

            Assert.AreEqual(12345678, result);
        }

        [Test]
        public void GettingFriendListByIdAvailable_ForUser()
        {
            var model = Mock.Of<IVkModel>(x => x.GetFriendList(12345678) == new List<string> {"John"});
            var srv = new VkService(model);

            var result = srv.Model.GetFriendList(12345678);

            CollectionAssert.AreEquivalent(new List<string> {"John"}, result);
        }

        [Test]
        public void GettingFriendIdListByIdAvailable_ForUser()
        {
            var model = Mock.Of<IVkModel>(x => x.GetFriendIdList(12345678) == new List<int> {1, 2, 3});
            var srv = new VkService(model);

            var result = srv.Model.GetFriendIdList(12345678);

            CollectionAssert.AreEquivalent(new List<int> {1, 2, 3}, result);
        }

        [Test]
        public void GettingAudioListByIdAvailable_ForUser()
        {
            var model = Mock.Of<IVkModel>(x => x.GetAudioList(12345678) == new List<string> {"Track1", "Track2"});
            var srv = new VkService(model);

            var result = srv.Model.GetAudioList(12345678);

            CollectionAssert.AreEquivalent(new List<string> {"Track1", "Track2"}, result);
        }

        #endregion

        #region Тесты поведения

        [Test]
        public void GettingPageUriByIdNotAvailable_ForNotAuthorizedUser()
        {
            var model = new VkModel();

            Assert.Throws<NotAuthorizedException>(() => model.GetUserPageUri(12345678));
        }

        [Test]
        public void GettingFullNameNotAvailable_ForNotAuthorizedUser()
        {
            var model = new VkModel();

            Assert.Throws<NotAuthorizedException>(() => model.GetFullName(12345678));
        }

        [Test]
        public void GettingPageUriByScreenNameNotAvailable_ForNotAuthorizedUser()
        {
            var model = new VkModel();

            Assert.Throws<NotAuthorizedException>(() => model.GetUserPageUri("jhon"));
        }

        [Test]
        public void GettingCurrentUserIdNotAvailable_ForNotAuthorizedUser()
        {
            var model = new VkModel();

            Assert.Throws<NotAuthorizedException>(() => model.GetCurrentUserId());
        }

        [Test]
        public void AuthorizationWithEmptyDataNotAvailable_ForUser()
        {
            var model = new VkModel();

            Assert.Throws<EmptyDataException>(() => model.Login(null, null, null));
        }

        [Test]
        public void SendingEmptyMessageNotAvailable_ForUser()
        {
            var model = new VkModel();

            Assert.Throws<EmptyDataException>(() => model.SendMessage(12345678, null));
        }

        [Test]
        public void SendingMessageToEmptyIdNotAvailable_ForUser()
        {
            var model = new VkModel();

            Assert.Throws<EmptyDataException>(() => model.SendMessage(null, "Sending message to null id is not available"));
        }

        [Test]
        public void GettingGroupsCountById_ForUser_12345678()
        {
            var mock = new Mock<IVkModel>();
            var srv = new VkService(mock.Object);

            srv.Model.GetGroupCount(12345678);

            mock.Verify(x => x.GetGroupCount(12345678));
        }

        [Test]
        public void GettingFriendListWasCalled_WithId_12345678()
        {
            var mock = new Mock<IVkModel>();
            var srv = new VkService(mock.Object);

            srv.Model.GetFriendList(12345678);

            mock.Verify(x => x.GetFriendList(12345678));
        }

        [Test]
        public void GettingAudiodListWasCalled_WithId_12345678()
        {
            var mock = new Mock<IVkModel>();
            var srv = new VkService(mock.Object);

            srv.Model.GetAudioList(12345678);

            mock.Verify(x => x.GetAudioList(12345678));
        }

        #endregion
    }
}