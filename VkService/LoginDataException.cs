﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VkInfo
{
    public class EmptyDataException : Exception
    {
        public EmptyDataException() : base()
        {

        }

        public EmptyDataException(string message) : base(message)
        {

        }
    }
}
