﻿using System;
using System.Collections.Generic;
using VkNet;
using VkNet.Enums.Filters;
using VkNet.Model;
using VkNet.Model.Attachments;
using VkNet.Model.RequestParams;

namespace VkInfo
{
    public interface IVkModel
    {
        bool Login(string login, string password, ulong? appId);
        long GetCurrentUserId();
        string GetFullName(long id);
        string GetUserPageUri(long id);
        string GetUserPageUri(string screenName);
        List<string> GetFriendList(long id);
        List<string> GetAudioList(long id);
        List<int> GetFriendIdList(long id);
        int GetGroupCount(long id);
        bool SendMessage(long? id, string message);
        string GetActivityByGroups(long id);

    }

    public class VkModel : IVkModel
    {
        private VkApi api;

        bool _authorized = false;
        public string GetActivityByGroups(long id)
        {
                int activity = GetGroupCount(id);

                if (activity <= 10 && activity != 0) { return "Низкая активность"; }
                if (activity >= 50 && activity < 100) { return "Средняя активность"; }
                if (activity >= 100) { return "Высокая активность"; }

                return "Нет активности";
        }

        public List<string> GetAudioList(long id)
        {
            User user;
            AudioGetParams param = new AudioGetParams();
            List<string> audioList = new List<string>();

            param.OwnerId = id;
            var data = api.Audio.Get(out user, param);

            foreach (var audio in data)
            {
                audioList.Add(audio.Title);
            }

            return audioList;
        }

        public long GetCurrentUserId()
        {
            if (_authorized)
            {
                return (long)api.UserId;
            }
            else
            {
                throw new NotAuthorizedException("You are not authorized");
            }
        }

        public List<int> GetFriendIdList(long id)
        {
            FriendsGetParams param = new FriendsGetParams();
            List<int> friendList = new List<int>();

            var data = api.Friends.Get(param);

            foreach (var friend in data)
            {
                friendList.Add((int)friend.Id);
            }

            return friendList;
        }

        public List<string> GetFriendList(long id)
        {
            FriendsGetParams param = new FriendsGetParams();
            List<string> friendList = new List<string>();

            var data = api.Friends.Get(param);

            foreach (var friend in data)
            {
                friendList.Add(friend.FirstName + " " + friend.LastName);
            }

            return friendList;
        }

        public string GetFullName(long id)
        {
            if (_authorized)
            {
                var data = api.Users.Get(id);

                return data.FirstName + " " + data.LastName;
            }
            else
            {
                throw new NotAuthorizedException("You are not authorized");
            }
        }

        public virtual int GetGroupCount(long id)
        {
            GroupsGetParams param = new GroupsGetParams();

            param.UserId = id;
            var data = api.Groups.Get(param);

            return data.Count;
        }

        public string GetUserPageUri(string screenName)
        {
            if (_authorized)
            {
                return "vk.com/" + screenName;
            }
            else
            {
                throw new NotAuthorizedException("You are not authorized");
            }
        }

        public string GetUserPageUri(long id)
        {
            if (_authorized)
            {
                return "vk.com/id" + id;
            }
            else
            {
                throw new NotAuthorizedException("You are not authorized");
            }
        }

        public bool Login(string login, string password, ulong? appId)
        {
            api = new VkApi();
            ApiAuthParams param = new ApiAuthParams();

            if (login == null || password == null || appId == null)
            {
                throw new EmptyDataException("Empty login data");
            }

            param.Login = login;
            param.Password = password;
            param.ApplicationId = (ulong)appId;
            param.Settings = Settings.All;

            try
            {
                api.Authorize(param);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка:" + ex.Message);
                return false;
            }

            _authorized = true;
            return _authorized;
        }

        public bool SendMessage(long? id, string message)
        {
            MessagesSendParams param = new MessagesSendParams();

            param.UserId = id;
            param.Message = message;

            if (id == null || message == null)
            {
                throw new EmptyDataException("Empty message data");
            }

            try
            {
                api.Messages.Send(param);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка:" + ex.Message);
                return false;
            }

            return true;
        }
    }

    public class VkService
    {
        public IVkModel Model;

        public VkService(IVkModel model)
        {
            Model = model;
        }

        public VkService()
        {
            Model = new VkModel();
        }

        public string GetActivityByGroups(long id)
        {
            return new VkService().GetActivityByGroups(id);
        }
    }
}
