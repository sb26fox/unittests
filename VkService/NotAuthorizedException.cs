﻿using System;
using System.Runtime.Serialization;

namespace VkInfo
{
    [Serializable]
    public class NotAuthorizedException : Exception
    {
        public NotAuthorizedException()
        {
        }

        public NotAuthorizedException(string message) : base(message)
        {
        }
    }
}